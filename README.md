# Piotr Krasun
## WHFV - about

Those pesky squares are striking again. This time you are here to fight back with your dank gun skills.
Survive as long as you can! The squares are going to spawn around you until your fail.

## Controls

WASD to move
Mouse Click to shoot

## Installation

Choose your favourite IDE to make things simpler.

Make sure all the sources are placed in your workspace.

Make sure that you have put /res/ into your working directory or made the root of the project the working directory.

Add jsfml.jar to your classpath you can find it here: http://jsfml.org/?page=download

or in /lib/

Compile.